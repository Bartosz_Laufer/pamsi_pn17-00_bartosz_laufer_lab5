#pragma once
#include <iostream>

using namespace std;

// funkcja zamieniajaca dwa elementy miejscami
template <typename Typ>
void zamien(Typ &a, Typ &b)
{
	Typ temp = a;
	a = b;
	b = temp;
}

// funkcja wyswietlajaca tablice
template <typename Typ>
void wyswietl(Typ tablica[], int lewy, int prawy)
{
	for (int i = lewy; i <= prawy; i++) {
		cout << tablica[i] << " ";
	}
	cout << endl;
}