#pragma once
#include <iostream>
#include <cstdlib>
#include "Pomocnicze.h"

using namespace std;

// sortowanie rosnace
template <typename Typ>
int partition(Typ tablica[], int lewy, int prawy, int piwot)
{
	int l = lewy; // indeks idacy od lewej strony
	int p = prawy - 1; // indeks idacy od prawej strony
	while(true) {
		while (tablica[l] < tablica[prawy]) // zatrzyma sie gdy z lewej strony piwotu znajdzie sie element niemniejszy od niego
			l++;
		while (tablica[p] > tablica[prawy]) // zatzyma sie gdy z prawej strony piwotu znajdzie sie element niewiekszy od niego
			p--;
		if (l < p) { // sprawdzenie czy indeksy sie nie "przeciely"
			zamien(tablica[l], tablica[p]); // zamiana miejscami dwoch elementow
			l++;
			p--;
		}
		else {
			zamien(tablica[prawy], tablica[l]); // zamiana piwotu (ostatniego elementu) z pierwszym elementem wiekszym od niego
			return l; // zwraca indeks w ktorym znajduje sie piwot
		}
	} 
}

template <typename Typ>
void quickSort(Typ tablica[], int lewy, int prawy)
{
	int nowyPiwot; // miejsce piwota po "przerzucaniu" elementow
	if (lewy <= prawy) {
		int piwot = lewy;// (rand() % (prawy - lewy + 1)) + lewy; // losowy element sposrod indeksow od lewy do prawy
		zamien(tablica[piwot], tablica[prawy]); // zamiana miejscami piwotu z ostatnim elementem
		nowyPiwot = partition(tablica, lewy, prawy, piwot); // sortowanie elementow wedlug piwotu
		quickSort(tablica, lewy, nowyPiwot - 1); // wywolanie rekurencyjne dla lewej strony piwotu
		quickSort(tablica, nowyPiwot + 1, prawy); // wywolanie rekurencyjne dla prawej strony piwotu
	}
}

// sortowanie malejace
template <typename Typ>
int partition2(Typ tablica[], int lewy, int prawy, int piwot)
{
	int l = lewy; // indeks idacy od lewej strony
	int p = prawy; // indeks idacy od prawej strony
	while (true) {
		while (tablica[l] > tablica[prawy]) // zatrzyma sie gdy z lewej strony piwotu znajdzie sie element niewiekszy od niego
			l++;
		while (tablica[p] < tablica[prawy]) // zatzyma sie gdy z prawej strony piwotu znajdzie sie element niemniejszy od niego
			p--;
		if (l < p) { // sprawdzenie czy indeksy sie nie "przeciely"
			zamien(tablica[l], tablica[p]); // zamiana miejscami dwoch elementow
			l++;
			p--;
		}
		else {
			zamien(tablica[prawy], tablica[l]); // zamiana piwotu (ostatniego elementu) z pierwszym elementem wiekszym od niego
			return l; // zwraca indeks w ktorym znajduje sie piwot
		}
	}
}

template <typename Typ>
void quickSort2(Typ tablica[], int lewy, int prawy)
{
	int nowyPiwot; // miejsce piwota po "przerzucaniu" elementow
	if (lewy <= prawy) {
		int piwot = (rand() % (prawy - lewy + 1)) + lewy; // losowy element sposrod indeksow od lewy do prawy
		zamien(tablica[piwot], tablica[prawy]); // zamiana miejscami piwotu z ostatnim elementem
		nowyPiwot = partition2(tablica, lewy, prawy, piwot); // sortowanie elementow wedlug piwotu
		quickSort2(tablica, lewy, nowyPiwot - 1); // wywolanie rekurencyjne dla lewej strony piwotu
		quickSort2(tablica, nowyPiwot + 1, prawy); // wywolanie rekurencyjne dla prawej strony piwotu
	}
}