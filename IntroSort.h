#pragma once
#include <iostream>
#include <cstdlib>
#include <math.h>
#include "Pomocnicze.h"
#include "QuickSort.h"

using namespace std;

template <typename Typ>
void insertionSort(Typ tablica[], int lewy, int prawy)
{
	for (int i = lewy + 1; i <= prawy; i++) {
		int key = tablica[i];
		int j = i - 1;
		while (j >= lewy  &&  tablica[j] > key) {
			tablica[j + 1] = tablica[j];
			j = j - 1;
		}
		tablica[j + 1] = key;
	}
} 
/*
template <typename Typ>
void heapify(Typ tablica[], int rozmiar, int i)
{
	if (i <= rozmiar / 2) {
		int largest = i; // liczba w indeksie dla ktorego wywolywana jest funkcja ustawiana jest jako obecnie najwieksza
		int l = 2 * i + 1; // lewy syn
		int r = 2 * i + 2; // prawy syn

		if (l < rozmiar && tablica[l] > tablica[largest])
			largest = l; // jesli lewy syn istnieje i jest wiekszy od largest ustawiany jest jako nowy najwiekszy element
		if (r < rozmiar && tablica[r] > tablica[largest])
			largest = r; // jesli prawy syn istnieje i jest wiekszy od largest ustawiany jest jako nowy najwiekszy element
		if (largest != i) { // jesli ktorys z synow zostal nowym largest
			zamien(tablica[i], tablica[largest]); // zamiana miejscami ojca z synem
			heapify(tablica, rozmiar, largest); // ponowne wywolanie funkcji dla nowego largest
			///cout << "i: " << i << "  3. ";
		}
	}
	///cout << "i: " << i << "  "; 
	///wyswietl(tablica, 0, rozmiar - 1);
} */

template <typename Typ>
void heapify(Typ tablica[], int rozmiar, int i)
{
	int l, r;
	int largest = i;
	while (i <= rozmiar / 2) {
		l = 2 * i + 1; // syn
		r = 2 * i + 2; // drugi syn 
		if (l < rozmiar && tablica[l] > tablica[largest])
			largest = l; // jesli lewy syn istnieje i jest wiekszy od largest ustawiany jest jako nowy najwiekszy element
		if (r < rozmiar && tablica[r] > tablica[largest])
			largest = r; // jesli prawy syn istnieje i jest wiekszy od largest ustawiany jest jako nowy najwiekszy element 
		if (largest != i) { // jesli ktorys z synow zostal nowym largest
			zamien(tablica[i], tablica[largest]);
			i = largest; // funkcja zadziala dla zamienionego elementu
		} 
		else
			break; 
	}
} 

template <typename Typ>
void heapSort(Typ tablica[], int iloscLiczb)
{
	// zbudowanie kopca z najwiekszym elementem u gory
	for (int i = iloscLiczb / 2 - 1; i >= 0; i--) {
		heapify(tablica, iloscLiczb, i);
	}
	// "usuwanie" elementow z kopca
	// element z gory idzie na koniec kopca ktorego rozmiar zostaje zmniejszony o 1
	for (int i = iloscLiczb - 1; i >= 0; i--) {
		zamien(tablica[0], tablica[i]); // zamienienie miejscami pierwszego i ostatniego elementu kopca
		heapify(tablica, i, 0); // wywolanie funkcji dla kopca o mniejszym rozmiarze
	}
}

template <typename Typ>
void introSort(Typ tablica[], int lewy, int prawy, int depth)
{
	if (lewy <= prawy) {
		/*
		if (prawy - lewy < 16) {
			insertionSort(tablica, lewy, prawy);
		} */
		if (depth != 0) { // po zrealizowaniu okreslonej liczby podzialow wywolana zostanie procedura heapsort
			int nowyPiwot;
			int piwot = (rand() % (prawy - lewy + 1)) + lewy; // losowy element sposrod indeksow od lewy do prawy
			zamien(tablica[piwot], tablica[prawy]); // zamiana miejscami piwotu z ostatnim elementem
			nowyPiwot = partition(tablica, lewy, prawy, piwot); // sortowanie elementow wedlug piwotu
			introSort(tablica, lewy, nowyPiwot - 1, depth - 1); // wywolanie rekurencyjne dla lewej strony piwotu
			introSort(tablica, nowyPiwot + 1, prawy, depth - 1); // wywolanie rekurencyjne dla prawej strony piwotu
		}
		else {
			heapSort(&tablica[lewy], (prawy - lewy + 1)); // referencja do pierwszego elementu danej czesci tablicy i rozmiar tej czesci tablicy
		}
	}
}

template <typename Typ>
void introspectiveSort(Typ tablica[], int iloscLiczb)
{
	int depth = 2 * log2(iloscLiczb); // po tylu podzialach uruchomiony zostanie heap sort
	introSort(tablica, 0, iloscLiczb, depth);
}