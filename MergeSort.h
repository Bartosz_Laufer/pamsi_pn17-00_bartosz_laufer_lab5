#pragma once
#include <iostream>
#include <cstdlib>

using namespace std;

// sortowanie rosnace
template <typename Typ>
void merge(Typ tablica[], int lewy, int srodek, int prawy)
{
	int rozmiarL = srodek - lewy + 1; // rozmiar "lewej" tablicy po podzieleniu tablicy pierwotnej na dwie czesci
	int rozmiarP = prawy - srodek; // rozmiar "prawej" tablicy
	Typ *tabL = new Typ[rozmiarL]; // utworzenie tablic pomocniczych
	Typ *tabP = new Typ[rozmiarP];

	// przepisanie tablicy do tablic pomocniczych
	for (int i = 0; i < rozmiarL; i++)
		tabL[i] = tablica[lewy + i];
	for (int j = 0; j < rozmiarP; j++)
		tabP[j] = tablica[srodek + 1 + j];

	// scalenie tablic
	int i = 0;
	int j = 0;
	int k = lewy; // indeks poczatkowy tablicy do ktorej wpisywane beda elementy
	while (i < rozmiarL && j < rozmiarP) { // dopoki obie tablice pomocnicze sa nie puste
		if (tabL[i] <= tabP[j]) { // mniejszy sposrod poczatkowych elementow tablic pomocniczych jest przepisywany do tablicy pierwotnej
			tablica[k] = tabL[i];
			i++; // przejscie do nastepnego elementu tabL
		}
		else {
			tablica[k] = tabP[j];
			j++; // przejscie do nastepnego elementu tabP
		}
		k++; // przejscie do nastepnego elementu tablicy pierwotnej
	}
	while (i < rozmiarL) { // gdy zostaly elementy tylko w "lewej" tablicy
		tablica[k] = tabL[i];
		i++;
		k++;
	}
	while (j < rozmiarP) { // gdy zostaly elementy tylko w "prawej" tablicy
		tablica[k] = tabP[j];
		j++;
		k++;
	}

	delete[] tabL; // zwolnienie pamieci
	delete[] tabP;
}

template <typename Typ>
void mergeSort(Typ tablica[], int lewy, int prawy)
{
	if (lewy < prawy)
	{
		int srodek = (lewy + prawy) / 2; // indeks srodka przedzialu 
		mergeSort(tablica, lewy, srodek); // podzial tablicy na czesci
		mergeSort(tablica, srodek + 1, prawy);
		merge(tablica, lewy, srodek, prawy); // scalenie tablicy
	}
}