#include "MergeSort.h"
#include "QuickSort.h"
#include "IntroSort.h"
#include "Pomocnicze.h"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <Windows.h>

// funkcja sprawdzajaca, czy tablica zostala poprawnie posortowana
template <typename Typ>
bool isSorted(Typ *tablica, int iloscLiczb)
{
	for (int i = 0; i < iloscLiczb-1; i++) {
		if (tablica[i] > tablica[i + 1])
			return false;
	}
	cout << "Posortowano poprawnie!\n";
	return true;
}

// funkcja sortujaca dany kawalek tablicy przed rzeczywistym sortowaniem
template <typename Typ>
void preSort(Typ *tablicaQ, Typ *tablicaM, Typ *tablicaI, int iloscLiczb)
{
	double ile; // procent elementow tablicy do posortowania
	cout << "Ile procent poczatkowych elementow tablicy ma byc posortowanych?: ";
	cin >> ile;
	if (ile < 0) {
		quickSort2(tablicaQ, 0, iloscLiczb - 1); // sortowanie tablic w odwrotnej kolejnosci
		quickSort2(tablicaM, 0, iloscLiczb - 1);
		quickSort2(tablicaI, 0, iloscLiczb - 1);
	}
	else if (ile != 0) { // gdy ile == 0 to tablice nie sa wczesniej sortowane
		quickSort(tablicaQ, 0, (iloscLiczb - 1) * (ile / 100.0)); // sortowanie odpowiedniej czesci tablic
		quickSort(tablicaM, 0, (iloscLiczb - 1) * (ile / 100.0));
		quickSort(tablicaI, 0, (iloscLiczb - 1) * (ile / 100.0));
	}
}

int main()
{
	LARGE_INTEGER frequency; // czestotliwosc
	LARGE_INTEGER t1, t2; // czas
	double Time; // wyliczony czas dzialania algorytmu
	srand(time(NULL));

	int iloscLiczb; // ilosc liczb do posortowania
	cout << "Podaj ilosc liczb do posortowania: ";
	cin >> iloscLiczb;
	int liczba; // wylosowana liczba

	int *tablicaQ = new int[iloscLiczb]; // tablica do sortowania szybkiego
	int *tablicaM = new int[iloscLiczb]; // tablica do sortowania przez scalanie
	int *tablicaI = new int[iloscLiczb]; // tablica do sortowania introspektywnego 

	for (int i = 0; i < iloscLiczb; i++) { // wypelnienie tablic i kolejki losowymi wartosciami
		liczba = rand() % 100;
		tablicaQ[i] = liczba;
		tablicaM[i] = liczba;
		tablicaI[i] = liczba;
	}

	preSort(tablicaQ, tablicaM, tablicaI, iloscLiczb);
	
	QueryPerformanceFrequency(&frequency); // pobranie czestotliwosci
	QueryPerformanceCounter(&t1); // uruchomienie timera
	///introspectiveSort(tablicaQ, iloscLiczb - 1);
	quickSort(tablicaQ, 0, iloscLiczb - 1);
	QueryPerformanceCounter(&t2); // zatrzymanie timera
	Time = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart; // obliczenie czasu
	cout << "Czas dla sortowania szybkiego tablicy: " << Time << " ms.\n";

	QueryPerformanceFrequency(&frequency); // pobranie czestotliwosci
	QueryPerformanceCounter(&t1); // uruchomienie timera
	///introspectiveSort(tablicaM, iloscLiczb - 1);
	mergeSort(tablicaM, 0, iloscLiczb - 1);
	QueryPerformanceCounter(&t2); // zatrzymanie timera
	Time = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart; // obliczenie czasu
	cout << "Czas dla sortowania przez scalanie tablicy: " << Time << " ms.\n";
	
	QueryPerformanceFrequency(&frequency); // pobranie czestotliwosci
	QueryPerformanceCounter(&t1); // uruchomienie timera
	///introspectiveSort(tablicaI, iloscLiczb - 1);
	introspectiveSort(tablicaI, iloscLiczb - 1);
	QueryPerformanceCounter(&t2); // zatrzymanie timera
	Time = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart; // obliczenie czasu
	cout << "Czas dla sortowania introspektywnego tablicy: " << Time << " ms.\n";

	isSorted(tablicaQ, iloscLiczb);
	isSorted(tablicaM, iloscLiczb);
	isSorted(tablicaI, iloscLiczb);

	// zwolnienie pamieci
	delete[] tablicaQ; 
	delete[] tablicaM;
	delete[] tablicaI;
}